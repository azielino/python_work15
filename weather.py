import sys
import os
from requests import request
import json
from datetime import datetime, timedelta


class WeatherForecast:

    HISTORY = "history.txt"
    BASE_URL = "https://weatherapi-com.p.rapidapi.com/forecast.json"
    QUERYSTRING = {"q":"Wroclaw","days":"3"}
    
    def __init__(self, api_key=sys.argv[1], check_date=str(datetime.today().date() + timedelta(1))):
        self.api_key = api_key
        self.headers = {
        'x-rapidapi-host': "weatherapi-com.p.rapidapi.com",
        'x-rapidapi-key': f"{self.api_key}"
        }
        self.check_date = check_date
        self.chance_dict = dict()
        self.history_info = self.check_history_file()

    def set_chance_of_rain(self):
        response = request("GET", self.BASE_URL , headers=self.headers, params=self.QUERYSTRING)
        response_json_dicts = json.loads(response.text)
        for api_dict in response_json_dicts['forecast']['forecastday']:
            if self.check_date == api_dict['date']:
                return api_dict['day']['daily_chance_of_rain']

    def add_forecast(self):
        self.chance_dict[self.check_date] = self.set_chance_of_rain()
        with open(self.HISTORY, "a") as file:
            file.write(f"{self.check_date}\n{self.chance_dict[self.check_date]}\n")

    def check_history_file(self):
        if os.path.exists(self.HISTORY):
            with open(self.HISTORY, 'r') as file:
                for row in file:
                    if self.check_date == row.replace("\n", ""):
                        self.chance_dict[self.check_date] = int(file.readline().replace("\n", ""))
                        return self.info(self.chance_dict[self.check_date])

    def info(self, chance):
        if chance < 20:
            return "Nie bedzie padac"
        elif chance > 80:
            return "Bedzie padac"
        return "Nie wiem"

    def __getitem__(self, index):
        if not self.history_info:
                self.add_forecast()
        return self.info(self.chance_dict[index])

    def items(self):
        with open(self.HISTORY, "r") as file:
            for row in file:
                date = row.replace("\n", "")
                weather = self.info(int(file.readline().replace("\n", "")))
                yield (date, weather)
    
    def __iter__(self):
            for tup in self.items():
                yield tup[0]


if len(sys.argv) == 2:
    wf = WeatherForecast()
else:
    wf = WeatherForecast(check_date=sys.argv[2])
print(wf[wf.check_date])
for tup in wf.items():
    print(tup)
for elem in wf:
    print(elem)